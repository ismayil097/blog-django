from django.shortcuts import render,HttpResponse


def home_view(request):
    if request.user.is_authenticated:
        context = {
            'name': 'Ismayil',
        }
    else:
        context = {
            'name': 'Qonag',
        }

    return render(request, 'home.html', context)