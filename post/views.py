from django.shortcuts import render, HttpResponse, get_object_or_404, HttpResponseRedirect, redirect
from .models import Post
from .forms import PostForm, CommentForm
from django.contrib import messages
from django.utils.text import slugify
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def post_index(request):
    post_list = Post.objects.all()
    paginator = Paginator(post_list, 6)

    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request, 'post/index.html', {'posts': posts})

def post_detail(request, slug):
    post = get_object_or_404(Post, slug=slug)

    form = CommentForm(request.POST or None)
    if form.is_valid():
        comment = form.save(commit=False)
        comment.post = post
        comment.save()
        return HttpResponseRedirect(post.get_absolute_url())

    context = {
        'post': post,
        'form': form,
    }
    return render(request, 'post/detail.html', context)

def post_create(request):

    # if request.method == 'POST':
    #     print(request.POST)

    # title = request.POST.get('title')
    # text = request.POST.get('text')
    # Post.objects.create(title=title,text=text)

    # if (request.method == 'POST'):
    #     # formdan gelen bilgileri kaydet
    #     form = PostForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #
    # else:
    #     # formu istifadeciye goster
    #     form = PostForm()

    if request.user.is_authenticated:
        form = PostForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()
            messages.success(request, 'post is created...')
            return HttpResponseRedirect(post.get_absolute_url())

        context = {
            'form': form,
        }

        return render(request, 'post/form.html', context)
    else:
        return HttpResponse("Are you clever?")

def post_update(request, slug):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, slug=slug)
        form = PostForm(request.POST or None, request.FILES or None, instance=post)
        if form.is_valid():
            form.save()
            messages.success(request, 'post is updated...')
            return HttpResponseRedirect(post.get_absolute_url())

        context = {
            'form': form,
        }

        return render(request, 'post/form.html', context)
    else:
        return HttpResponse("Are you clever???")

def post_delete(request,id):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, id=id)
        post.delete()
        return redirect('post:index')

    else:
        return HttpResponse("Are you clever???")

